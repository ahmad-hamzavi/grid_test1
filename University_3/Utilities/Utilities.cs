﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using University_3.Models;
using UniversityDBProvider;

namespace University_3.Utilities
{
    public static class Utilities
    {
        public static string ConvertToDate(this DateObject date)
        {
            string datestr = date.Year + "/" +
                date.Month + "/" +
                date.Day;
            return datestr.ShamsiToDateTime().ToString();
        }
    }
}