﻿using System.Collections.Generic;
using UniversityEntity;

namespace University_3.ViewModel
{
    public class AddProfessorViewModel
    {
        public IEnumerable<Field> Fields { get; set; }
        public IEnumerable<State> States { get; set; }
        public IEnumerable<Town> Townes { get; set; }
    }
}