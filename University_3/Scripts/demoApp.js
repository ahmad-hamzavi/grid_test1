angular.module('myApp', ['dataGrid', 'pagination', 'ngMaterial'])
    .controller('myAppController', ['$scope', 'myAppFactory', function ($scope, myAppFactory) {

        $scope.gridOptions = {
            data: [],
            urlSync: true
        };
        myAppFactory.getData().then(function (responseData) {
            $scope.gridOptions.data = responseData.data;
            //$scope.paginationOptions.totalItems = 100;
            console.log($scope.paginationOptions);
        });

    }])
    .factory('myAppFactory', function ($http) {
        return {
            getData: function () {
                return $http({
                    method: 'POST',
                    url: 'http://localhost:24628/Professor/AllProfessors3'
                    //method: 'GET',
                    //url: 'http://angular-data-grid.github.io/demo/data.json'

                });
            }
        }
    });

