﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using University_3.Models;
using University_3.Utilities;
using University_3.ViewModel;
using UniversityDBProvider;
using UniversityEntity;
using UniversityLogic;

namespace University_3.Controllers
{
    public class ProfessorController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Index2()
        {
            return View();
        }
        public ActionResult Index3()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AllProfessors3()
        {
            ModelStateDictionary md = ModelState;
            ProfessorManager professorManager = new ProfessorManager();
            return Json(professorManager.GetProfessorsPage3());
        }
        [HttpPost]
        public ActionResult AllProfessors2()
        {
            ProfessorManager professorManager = new ProfessorManager();
            return Json(professorManager.GetProfessorsPage2());
        }
        [HttpPost]
        public ActionResult AllProfessors(int rowCount, int current)
        {
            ProfessorManager professorManager = new ProfessorManager();
            return Json(professorManager.GetProfessorsPage(rowCount, current));
        }
        [HttpPost]
        public ActionResult Choices(int? id)
        {

            IList<string> choices = new List<string>();
            for (int i = 0; i < 10; i++)
            {
                choices.Add("choice" + i);
            }
            return Json(choices);
        }
        [HttpPost]
        public ActionResult Details(int? id)
        {

            string detail = "detail"+ id.ToString();
            
            return Content(detail);
        }

        public ActionResult AddNewProfessor()
        {
            FieldManager fieldManager = new FieldManager();
            StateManager stateManager = new StateManager();
            TownManager townManager = new TownManager();
            AddProfessorViewModel addProfessorViewModel = new AddProfessorViewModel();
            addProfessorViewModel.Fields = fieldManager.ReadAll();
            addProfessorViewModel.States = stateManager.ReadAll();
            addProfessorViewModel.Townes = townManager.ReadAll();
            return View(addProfessorViewModel);
        }
        [HttpPost]
        public ActionResult AddNewProfessor(ProfessorModel professorModel)
        {
            ProfessorManager professorManger = new ProfessorManager();
            CourseManager courseManager = new CourseManager();
            StateManager stateManager = new StateManager();
            FieldManager fieldManager = new FieldManager();
            TownManager townManager = new TownManager();
            List<Course> courses = new List<Course>();
            State state = stateManager.Read(professorModel.State);
            Town town = townManager.Read(professorModel.Town);
            Field field = fieldManager.Read(professorModel.Field);
            courses = courseManager.ReadAll();

            Professor professor = new Professor()
            {
                FirstName = professorModel.FirstName,
                LastName = professorModel.LastName,
                Emial = professorModel.Email,
                StartContractDate = professorModel.StartDate.ConvertToDate(),
                EndContractDate = professorModel.EndDate.ConvertToDate(),
                ContrcatType = professorModel.ContractType,
                Field = field,
                State = state,
                Town = town
            };
            if (!string.IsNullOrWhiteSpace(professorModel.Courses))
            {
                professorManger.AddCourses(professor, professorModel.Courses);
            }

            professorManger.Add(professor);
            professorManger.saveChanges();
            return RedirectToAction("AddNewProfessor");

        }



        [HttpPost]
        public ActionResult Getcourses(int rowCount, int current)
        {
            CourseManager courseManager = new CourseManager();
            return Json(courseManager.GetCoursePage(rowCount, current));
        }
        public string AddBulkProfessor()
        {
            ProfessorManager professorManager = new ProfessorManager();
            List<Professor> professors = new List<Professor>();
            int count = professorManager.Count;
            Professor professor;
            for (int i = count; i <= count + 10000; i++)
            {
                professor = new Professor
                {
                    FirstName = "احمد " + i.ToString(),
                    LastName = "حمزوی " + i.ToString()
                };
                professors.Add(professor);
            }
            professorManager.saveChanges();
            return "OK";
        }
    }
}