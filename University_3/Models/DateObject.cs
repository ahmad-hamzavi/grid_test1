﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace University_3.Models
{
    public class DateObject
    {
            public int Day { get; set; }
            public int Month { get; set; }
            public int Year { get; set; }

    }
}