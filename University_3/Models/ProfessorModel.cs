﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace University_3.Models
{
    public class ProfessorModel
    {
        public string Courses { get; set; }
        public int State { get; set; }
        public int Town { get; set; }
        public int Field { get; set; }
        public int daysOfWeek { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public ContrcatType ContractType { get; set; }
        public DateObject StartDate { get; set; }
        public DateObject EndDate { get; set; }
    }
  
}
