﻿using UniversityEntity;
using UniversityDBProvider;
using UniversityDataAccessLayer;
using System.Data.Entity;

namespace UniversityLogic
{
    public class FieldManager : DBProvider<Field>
    {
        public FieldManager() : base(new DatabaseContext())
        {
        }
    }
}
