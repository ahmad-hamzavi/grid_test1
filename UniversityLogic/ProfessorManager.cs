﻿using UniversityDBProvider;
using System.Collections.Generic;
using System.Linq;
using UniversityDataAccessLayer;
using UniversityEntity;
using System;

namespace UniversityLogic
{
    public class ProfessorManager : DBProvider<Professor>
    {
        public ProfessorManager() : base(new DatabaseContext())
        {

        }
        public void AddCourses(Professor professor, string strCourses)
        {
            List<int> listCourse = new List<int>();
            List<string> listStrCourses = new List<string>();
            if (strCourses.Contains(','))
            {
                listStrCourses = strCourses.Split(',').ToList();
            }
            else
            {
                listStrCourses.Add(strCourses);
            }
            foreach (var item in listStrCourses)
            {
                listCourse.Add(Convert.ToInt32(item));
            }
            professor.Courses = new List<Course>();
            var courseDBset = DBC.Set<Course>();
            foreach (var item in listCourse)
            {
                professor.Courses.Add(courseDBset.FirstOrDefault(x => x.Id == item));
            }
        }

        public BootgridProfessor GetProfessors()
        {
            BootgridProfessor bootgridObject = new BootgridProfessor();

            List<Professor> Professors = new List<Professor>();
            List<ProfessorBootgridRow> bootgridRows = new List<ProfessorBootgridRow>();
            Professors = ReadAll().ToList();
            foreach (var item in Professors)
            {
                ProfessorBootgridRow bootgridRow = new ProfessorBootgridRow();
                bootgridRow.id = item.Id;
                bootgridRow.FirstName = item.FirstName;
                bootgridRow.LastName = item.LastName;
                bootgridRows.Add(bootgridRow);
            }

            bootgridObject.current = 1;
            bootgridObject.rowCount = 10;
            bootgridObject.rows = bootgridRows;
            bootgridObject.rowCount = Professors.Count();
            return bootgridObject;
        }
        public BootgridProfessor GetProfessorsPage(int rowCount, int current)

        {
            BootgridProfessor bootgridObject = new BootgridProfessor();
            var stateDBset = DBC.Set<State>();
            var townDBset = DBC.Set<Town>();
            var fieldDBset = DBC.Set<Field>();
            List<Professor> Professors = new List<Professor>();
            List<ProfessorBootgridRow> bootgridRows = new List<ProfessorBootgridRow>();
            IList<string> choices = new List<string>();
            for (int i = 0; i < 10; i++)
            {
                choices.Add("choice" + i);
            }
            int total;
            Professors = Read(current, rowCount, x => x.Id, true, out total).ToList();
            foreach (var item in Professors)
            {

                ProfessorBootgridRow bootgridRow = new ProfessorBootgridRow()
                {
                    id = item.Id,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    ContractType = item.ContrcatType.ToString(),


                };
                bootgridRow.Choices = choices;
                bootgridRows.Add(bootgridRow);
            }


            bootgridObject.current = current;
            bootgridObject.rowCount = rowCount;
            bootgridObject.rows = bootgridRows;
            bootgridObject.total = total;
            return bootgridObject;
        }

        public BootgridProfessor GetProfessorsPage2()

        {
            BootgridProfessor bootgridObject = new BootgridProfessor();
            var stateDBset = DBC.Set<State>();
            var townDBset = DBC.Set<Town>();
            var fieldDBset = DBC.Set<Field>();
            List<Professor> Professors = new List<Professor>();
            List<ProfessorBootgridRow> bootgridRows = new List<ProfessorBootgridRow>();
            IList<string> choices = new List<string>();
            for (int i = 0; i < 10; i++)
            {
                choices.Add("choice" + i);
            }
            int total;
            Professors = Read(1, 10, x => x.Id, true, out total).ToList();
            foreach (var item in Professors)
            {

                ProfessorBootgridRow bootgridRow = new ProfessorBootgridRow()
                {
                    id = item.Id,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    ContractType = item.ContrcatType.ToString(),


                };
                bootgridRow.Choices = choices;
                bootgridRows.Add(bootgridRow);
            }


            bootgridObject.current = 1;
            bootgridObject.rowCount = 10;
            bootgridObject.rows = bootgridRows;
            bootgridObject.total = total;
            return bootgridObject;
        }

        public List<ProfessorBootgridRow> GetProfessorsPage3()

        {
            BootgridProfessor bootgridObject = new BootgridProfessor();
            var stateDBset = DBC.Set<State>();
            State state = stateDBset.FirstOrDefault();
            var townDBset = DBC.Set<Town>();
            var fieldDBset = DBC.Set<Field>();
            List<Professor> Professors = new List<Professor>();
            List<ProfessorBootgridRow> bootgridRows = new List<ProfessorBootgridRow>();
            List<string> choices = new List<string>();
            for (int i = 0; i < 10; i++)
            {
                choices.Add("choice" + i);
            }
            int total;
            Professors = Read(1, 5000, x => x.Id, true, out total).ToList();
            foreach (var item in Professors)
            {

                ProfessorBootgridRow bootgridRow = new ProfessorBootgridRow()
                {
                    id = item.Id,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    ContractType = item.ContrcatType.ToString(),


                };
                if (state != null)
                    bootgridRow.State = state.StateName;
                bootgridRow.Choices = choices;
                bootgridRows.Add(bootgridRow);
            }


            bootgridObject.current = 1;
            bootgridObject.rowCount = 10;
            bootgridObject.rows = bootgridRows;
            bootgridObject.total = total;
            return bootgridRows;
        }
        public class BootgridProfessor
        {
            public int current { get; set; }
            public int rowCount { get; set; }
            public int total { get; set; }
            public List<ProfessorBootgridRow> rows { get; set; }
        }
        public class ProfessorBootgridRow
        {
            public int id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string State { get; set; }
            public string Town { get; set; }
            public string Field { get; set; }
            public string ContractType { get; set; }
            public string StartContractDate { get; set; }
            public string EndContractDate { get; set; }
            public IList<string> Choices { get; set; }
        }

    }
}
