﻿using UniversityDBProvider;
using System.Collections.Generic;
using System.Linq;
using UniversityDataAccessLayer;
using UniversityEntity;

namespace UniversityLogic
{
    public class CourseManager : DBProvider<Course>
    {
        public CourseManager() : base(new DatabaseContext())
        {

        }
        public BootgridCourse GetCourses()
        {
            BootgridCourse bootgridCourse = new BootgridCourse();
            
            List<Course> courses = new List<Course>();
            List<BootgridRowCourse> bootgridRows = new List<BootgridRowCourse>();
            courses = ReadAll().ToList();
            foreach (var item in courses)
            {
                BootgridRowCourse bootgridRow = new BootgridRowCourse();
                bootgridRow.id = item.Id;
                bootgridRow.course = item.CourseName;
                bootgridRows.Add(bootgridRow);
            }
        
            bootgridCourse.current = 1;
            bootgridCourse.rowCount = 10;
            bootgridCourse.rows = bootgridRows;
            bootgridCourse.rowCount = courses.Count();
            return bootgridCourse;
        }
        public BootgridCourse GetCoursePage(int rowCount,int current)
        {
            BootgridCourse bootgridCourse = new BootgridCourse();

            List<Course> courses = new List<Course>();
            List<BootgridRowCourse> bootgridRows = new List<BootgridRowCourse>();
            int total;
            courses = Read(current,rowCount,x=>x.Id,true,out total).ToList();
            foreach (var item in courses)
            {
                BootgridRowCourse bootgridRow = new BootgridRowCourse();
                bootgridRow.id = item.Id;
                bootgridRow.course = item.CourseName;
                bootgridRows.Add(bootgridRow);
            }

            bootgridCourse.current = current;
            bootgridCourse.rowCount = rowCount;
            bootgridCourse.rows = bootgridRows;
            bootgridCourse.total = total;
            return bootgridCourse;
        }
    }
    public class BootgridCourse
    {
        public int current { get; set; }
        public int rowCount { get; set; }
        public int total { get; set; }
        public List<BootgridRowCourse> rows { get; set; }
    }
    public class BootgridRowCourse
    {
        public int id { get; set; }
        public string course { get; set; }
        public string daysOfWeek { get; set; }
                   
    }

}
