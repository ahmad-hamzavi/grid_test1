﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using UniversityEntity;

namespace UniversityDBProvider
{
    public class DBProvider<T> where T : EntityBaseModel
    {
        protected DbSet<T> dbSet;
        protected DbContext DBC;
        /// <summary>
        /// Constractor for initilizing Database Context
        /// </summary>
        /// <param name="DBC"></param>
        public DBProvider(DbContext DBC)
        {
            this.DBC = DBC;
            this.dbSet = DBC.Set<T>();
        }
        public void Add(T Entity)
        {
            dbSet.Add(Entity);
        }
        public void Add(List<T> Entities)
        {
            foreach (T Entity in Entities)
            {
                dbSet.Add(Entity);
            }
        }
        public int Count
        {
            get
            {
                return dbSet.Count();
            }
        }
        public List<T> ReadAll()
        {
            return dbSet.AsNoTracking().ToList<T>();
        }
        public T Read(int Id)
        {
            //return DBC.Set<T>().Where(x => x.I
            return dbSet.Find(Id);
        }
        public List<T> Read(Expression<Func<T, bool>> filter = null)
        {
            DBC.Configuration.LazyLoadingEnabled = false;
            if (filter != null)
            {
                return dbSet.Where(filter).ToList();
            }
            else
            {
                return dbSet.ToList();
            }
        }
        public IEnumerable<T> Read(Expression<Func<T, bool>> filter = null,
                                            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
                                            string includeProperties = "")
        {
            DBC.Configuration.LazyLoadingEnabled = false;
            IQueryable<T> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }
        public IEnumerable<T> Read(string filter,
                                    string includeProperties = "")
        {
            DBC.Configuration.LazyLoadingEnabled = true;
            IQueryable<T> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }
            if (!string.IsNullOrEmpty(includeProperties))
            {
                if (includeProperties.Contains(','))
                {
                    foreach (var includeProperty in includeProperties.Split
                        (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        query = query.Include(includeProperty);
                    }
                }
                else
                {
                    query = query.Include(includeProperties);
                }
            }
            return query.ToList();
        }
        public IEnumerable<T> Read<TResult>(int pageNumber, int pageSize, Expression<Func<T, TResult>> orderByModel, bool isAscendingOrder,  out int rowsCount,string includeProperties = "")
        {
            DBC.Configuration.LazyLoadingEnabled = true;
            IQueryable<T> query = dbSet;
            if (pageSize <= 0) pageSize = 20;
            //Total result count
            rowsCount = query.Count();
            if (!string.IsNullOrWhiteSpace(includeProperties))
            {
                if (includeProperties.Contains(','))
                {
                    foreach (var includeProperty in includeProperties.Split
                        (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        query = query.Include(includeProperty);
                    }
                }
                else
                {
                    query = query.Include(includeProperties);
                }
            }
            //If page number should be > 0 else set to first page
            if (rowsCount <= pageSize || pageNumber <= 0) pageNumber = 1;
            //Calculate nunber of rows to skip on pagesize
            int excludedRows = (pageNumber - 1) * pageSize;
            //Skip the required rows for the current page and take the next records of pagesize count
            query = isAscendingOrder ? query.OrderBy(orderByModel) : query.OrderByDescending(orderByModel);
            return query.Skip(excludedRows).Take(pageSize);
        }
        public IEnumerable<T> Read<TResult>(IQueryable<T> query, int pageNumber, int pageSize, Expression<Func<T, TResult>> orderByModel, bool isAscendingOrder, out int rowsCount)
        {
            DBC.Configuration.LazyLoadingEnabled = false;
            if (pageSize <= 0) pageSize = 20;
            //Total result count
            rowsCount = query.Count();
            //If page number should be > 0 else set to first page
            if (rowsCount <= pageSize || pageNumber <= 0) pageNumber = 1;
            //Calculate nunber of rows to skip on pagesize
            int excludedRows = (pageNumber - 1) * pageSize;
            query = isAscendingOrder ? query.OrderBy(orderByModel) : query.OrderByDescending(orderByModel);
            //Skip the required rows for the current page and take the next records of pagesize count
            return query.Skip(excludedRows).Take(pageSize).AsNoTracking();
        }

        public void Remove(T Entity)
        {
            DBC.Entry(Entity).State = EntityState.Deleted;
        }
        public void RemoveRange(List<T> Entities)
        {
            dbSet.RemoveRange(Entities);
        }
        public void Update(T Entity)
        {
            DBC.Entry(Entity).State = EntityState.Modified;

        }
        public void Update(List<T> Entities)
        {
            foreach (T Entity in Entities)
            {
                DBC.Entry(Entity).State = EntityState.Modified;
            }
        }
        public void saveChanges()
        {
            DBC.SaveChanges();
            Dispose();

        }

        public void saveChangesWithoutDispose()
        {
            DBC.SaveChanges();
        }
        public void Dispose()
        {
            DBC?.Dispose();
        }
    }
}
