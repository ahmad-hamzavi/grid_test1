﻿using System.Data.Entity;
using UniversityEntity;

namespace UniversityDataAccessLayer
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext()
        {
            SetDatabaseInitializer.Set();
        }
        public DbSet<Course> Courses { get; set; }

        public DbSet<Professor> Professors { get; set; }
        public DbSet<DaysOfweek> DaysOfweek { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<Town> Townes { get; set; }
        public DbSet<Field> Fields { get; set; }
    }
}
