﻿using System.Data.Entity;

namespace UniversityDataAccessLayer
{
    public static class SetDatabaseInitializer
    {
        public static void Set()
        {
            Database.SetInitializer(new DatabaseInitializer());
        }
    }
}
