﻿using System.Linq;
using System.Data.Entity;
using UniversityEntity;
using System;
using EntityFramework.BulkInsert.Extensions;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace UniversityDataAccessLayer
{
    class DatabaseInitializer : DropCreateDatabaseIfModelChanges<DatabaseContext>
    {
        public DatabaseInitializer()
        {

        }
        protected override void Seed(DatabaseContext context)
        {
            //context.DaysOfweek.Add(new DaysOfweek { Name="1"})
            context.Configuration.AutoDetectChangesEnabled = false;
            context.Configuration.ValidateOnSaveEnabled = false;

            for (int i = 1; i <= 20; i++)
            {
                context.Courses.Add(new Course() { CourseName = " درس " + i.ToString() });
            }
            for (int i = 1; i <= 29; i++)
            {
                context.States.Add(new State() { StateName = " استان " + i.ToString() });
            }
            for (int i = 1; i <= 20; i++)
            {
                context.Townes.Add(new Town() { TownName = " شهراستان " + i.ToString(), State = context.States.FirstOrDefault() });
            }
            for (int i = 1; i <= 20; i++)
            {
                context.Fields.Add(new Field() { FieldName = " رشته تحصیلی " + i.ToString() });
            }
            context.SaveChanges();
            context.Dispose();
            context = null;
            context = new DatabaseContext();
            List<State> states=new List<State>();
            List<Town> townes=new List<Town>();
            List<Field> fields=new List<Field>();
            int ii = 1;
            ICollection<Professor> professors = new Collection<Professor>();
            for (int y = 1; y <= 300; y++)
            {

                if (professors == null)
                    professors = new Collection<Professor>();
                if (context == null)
                {
                     context = new DatabaseContext();
                     states = context.States.ToList();
                     townes = context.Townes.ToList();
                     fields = context.Fields.ToList();
                }
                for (int i = 1; i <= 10000; i++)
                {
                    ii++;

                    Professor professor = new Professor()
                    {
                        FirstName = "احمد " + ii.ToString(),
                        LastName = "حمزوی " + ii.ToString(),

                        StartContractDate = DateTime.Now.ToString(),
                        EndContractDate = DateTime.Now.ToString()
                    };
                    professor.State = states.FirstOrDefault();
                    professor.Town = townes.FirstOrDefault();
                    professor.Field = fields.FirstOrDefault();
                    professors.Add(professor);
                }
                context.BulkInsert(professors);
                context.SaveChanges();
                context.Dispose();
                context = null;
                professors = null;
            }

            //for (int y = 1; y <= 20000; y++)
            //{
            //    if (context == null)
            //        context = new DatabaseContext();
            //    for (int i = 1; i <= 1000; i++)
            //    {
            //        ii++;

            //        Professor professor = new Professor()
            //        {
            //            FirstName = "احمد " + ii.ToString(),
            //            LastName = "حمزوی " + ii.ToString()
            //        };

            //        professors.Add(professor);
            //    }
            //    context.Professors.AddRange(professors);
            //    context.SaveChanges();
            //    context.Dispose();
            //    context = null;
            //}
            //context.Configuration.AutoDetectChangesEnabled = true;
            //context.Configuration.ValidateOnSaveEnabled = true;
        }

    }
}
