﻿namespace UniversityEntity
{
    public abstract class  BaseIndividualInformations : EntityBaseModel
    {      
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Emial { get; set; }
    }
}
