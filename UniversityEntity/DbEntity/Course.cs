﻿using System.Collections.Generic;

namespace UniversityEntity
{
    public class Course:EntityBaseModel
    {
        public string  CourseName { get; set; }
        public ICollection<Professor> Professors { get; set; }
    }
}
