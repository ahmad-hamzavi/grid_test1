﻿namespace UniversityEntity
{
    public class Town:EntityBaseModel
    {
        public string TownName { get; set; }
        public State State { get; set; }
    }
}
