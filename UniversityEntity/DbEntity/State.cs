﻿using System.Collections.Generic;

namespace UniversityEntity
{
    public class State:EntityBaseModel
    {
        public string StateName { get; set; }
        public ICollection<Town> Townes { get; set; }
    }
}
