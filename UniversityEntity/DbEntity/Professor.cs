﻿using System;
using System.Collections.Generic;

namespace UniversityEntity
{
    public class Professor:BaseIndividualInformations
    {
        public virtual ICollection<Course> Courses { get; set; }
        public virtual State State { get; set; }
        public virtual Town Town { get; set; }
        public virtual Field Field { get; set; }
        public ContrcatType ContrcatType { get; set; }
        public string StartContractDate { get; set; }
        public string EndContractDate { get; set; }
    }
}
